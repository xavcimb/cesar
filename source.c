// code de cesar

/**
*  ASR => M2101                                                               *
***
*                                                                             *
*  N� de Sujet : 3                                                             *
*                                                                             *
***
*                                                                             *
*  Intitul� : Chiffrement de messages                                                              *
*                                                                             *
***
*                                                                             *
*  Nom-pr�nom1 :  Cimber Xavier                                                            *
*                                                                             *
*  Nom-pr�nom2 :  Batista Fabrice                                                            *
*                                                                             *
***
*                                                                             *
*  Nom du fichier :  Cesar                                                         *
*                                                                             *
**/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "Ent�te.h"

// programme principal
int main()
{
    int decalage = 0;
    char texte[256];
    char choix = 'c';
    // choix
    printf( "Code de Cesar\n" );
    printf( "\nSaisir le d�calage : " );
    scanf( "%d", &decalage );
    finDEntree();
    printf( "\nSaisir le texte : " );
    scanf( "%s", texte );
    finDEntree();
    if(verifierAlphaNumerique(texte) == 'f')
        printf("erreur caract�re sp�cial trouv� dans le texte %s\n", texte );
    else
        printf( "\nChiffrement [c] ou d�chiffrement [d] : " );
        scanf( "%c", &choix );
        // synth�se
        switch( choix )
        {
        case 'c' :
            printf("\nVous avez demand� le chiffrement du fichier %s avec un d�calage de %d.\n", texte, decalage );
            {
                chiffrer(texte, decalage);
                printf("%s", texte );
            }
            return 0;

        case 'd' :
            printf("\nVous avez demand� le d�chiffrement du fichier %s avec un d�calage de %d.\n", texte, decalage );
            {
                dechiffrer(texte, decalage);
                printf("%s", texte );
            }
            return 0;
        default:
            return -1;
    }
}

