/**
*  ASR => M2101                                                               *
***
*                                                                             *
*  N� de Sujet : 3                                                             *
*                                                                             *
***
*                                                                             *
*  Intitul� : Chiffrement de messages                                                              *
*                                                                             *
***
*                                                                             *
*  Nom-pr�nom1 :  Cimber Xavier                                                            *
*                                                                             *
*  Nom-pr�nom2 :  Batista Fabrice                                                            *
*                                                                             *
***
*                                                                             *
*  Nom du fichier :  Cesar                                                         *
*                                                                             *
**/

// Signature
/*finDEntree permet � l'utilisateur de continuer l'entr�e d'un caract�re
jusqu'� ce qu'il presse la touche entr�e*/
void finDEntree();

/*verifierAlphaNumerique prend une chaine de caract�res texte
retourne un caract�re trouve*/
char verifierAlphaNumerique(char* texte);

/*chiffrer prend en entr�e une chaine de caract�re texte et un entier decalage
retourne une chaine de caract�re*/
char chiffrer(char* texte, int decalage);

/*dechiffrer prend en entr�e une chaine de caract�re texte et un entier decalage
retourne une chaine de caract�re*/
char dechiffrer(char* texte, int decalage);

//Fonction
void finDEntree()
{
    while( getchar() != '\n' )
        continue;
}

char verifierAlphaNumerique(char* texte)
{
    char trouve;
    for(int i =0; i< strlen(texte); i++)
        if(!isalnum(texte[i]))
            trouve = 'f';
    return trouve;
}

char chiffrer(char* texte, int decalage)
{
    // d�chiffrement de texte
    for( int i = 0; i != strlen(texte); ++i )
    {
        // chiffrement des majuscules
        if( texte[ i ] >=65 && texte[ i ] <= 90 )
            texte[ i ] = ( ( texte[ i ] - 65 ) + decalage ) % 26 + 65;
        // chiffrement des minuscules
        if( texte[ i ] >=97 && texte[ i ] <= 122 )
            texte[ i ] = ( ( texte[ i ] - 97 ) + decalage ) % 26 + 97;
    }
    return texte;
}

char dechiffrer(char* texte, int decalage)
{
    // d�chiffrement de texte
    for( int i = 0; i != strlen(texte); ++i )
    {
        // d�chiffrement des majuscules
        if( texte[ i ] >=65 && texte[ i ] <= 90 )
            texte[ i ] = ( ( texte[ i ] - 65 ) - decalage ) % 26 + 65;
        // d�chiffrement des minuscules
        if( texte[ i ] >=97 && texte[ i ] <= 122 )
            texte[ i ] = ( ( texte[ i ] - 97 ) - decalage ) % 26 + 97;
    }
    return texte;
}
