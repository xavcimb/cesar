L'objectif de notre application est de pouvoir, à l'aide du chiffrement de César, chiffrer ou déchiffrer un message alphanumérique.

Cette application est développée par :
Batista Fabrice et Cimber Xavier

Documentation des fonctions :

	finDEntree permet à l'utilisateur de continuer l'entrée d'un caractère
	jusqu'à ce qu'il presse la touche entrée
	
	verifierAlphaNumerique prend une chaine de caractères texte
	retourne un caractère trouve
	
	chiffrer prend en entrée une chaine de caractère texte et un entier decalage
	retourne une chaine de caractère

	dechiffrer prend en entrée une chaine de caractère texte et un entier decalage
	retourne une chaine de caractère

Cas d'erreur :

Si le message contient des caractères spéciaux.
Si le décalage contient des lettres.
Si la taille de la chaine de caractère est dépassée.
